import { Plugin } from '@ckeditor/ckeditor5-core';
import InsertTwoColumnCommand from "./InsertTwoColumnCommand";
import { toWidget, toWidgetEditable } from '@ckeditor/ckeditor5-widget/src/utils';
import Widget from '@ckeditor/ckeditor5-widget/src/widget';

export default class TwoColumnEditing extends Plugin {
    static get requires() {
        return [ Widget ];
    }

    init() {
        console.log( 'TwoColumnEditing#init() got called' );

        this._defineSchema();
        this._defineConverters();

        this.editor.commands.add( 'insertTwoColumn', new InsertTwoColumnCommand( this.editor ) );
    }

    _defineSchema() {
        const schema = this.editor.model.schema;

        schema.register( 'twoColumn', {
            // Behaves like a self-contained object (e.g. an image).
            isObject: true,

            // Allow in places where other blocks are allowed (e.g. directly in the root).
            allowWhere: '$block'
        } );

        schema.register( 'twoColumnFirst', {
            // Cannot be split or left by the caret.
            isLimit: true,

            allowIn: 'twoColumn',

            // Allow content which is allowed in blocks (i.e. text with attributes).
            allowContentOf: '$root'
        } );

        schema.register( 'twoColumnSecond', {
            // Cannot be split or left by the caret.
            isLimit: true,

            allowIn: 'twoColumn',

            // Allow content which is allowed in the root (e.g. paragraphs).
            allowContentOf: '$root'
        } );

        schema.addChildCheck( ( context, childDefinition ) => {
            if ( context.endsWith( 'twoColumnSecond' ) && childDefinition.name == 'twoColumn' ) {
                return false;
            }
        } );
    }

    _defineConverters() {
        const conversion = this.editor.conversion;

        // <simpleBox> converters
        conversion.for( 'upcast' ).elementToElement( {
            model: 'twoColumn',
            view: {
                name: 'div',
                classes: ['row', 'ck-twocolumns']
            }
        } );
        conversion.for( 'dataDowncast' ).elementToElement( {
            model: 'twoColumn',
            view: {
                name: 'div',
                classes: ['row', 'ck-twocolumns']
            }
        } );
        conversion.for( 'editingDowncast' ).elementToElement( {
            model: 'twoColumn',
            view: ( modelElement, { writer: viewWriter } ) => {
                const section = viewWriter.createContainerElement( 'section', { class: 'row ck-twocolumns' } );

                return toWidget( section, viewWriter, { label: 'Two column widget' } );
            }
        } );

        // <simpleBoxTitle> converters
        conversion.for( 'upcast' ).elementToElement( {
            model: 'twoColumnFirst',
            view: {
                name: 'div',
                classes: ['col-12', 'col-md-6', 'ck-twocolumns_first']
            }
        } );
        conversion.for( 'dataDowncast' ).elementToElement( {
            model: 'twoColumnFirst',
            view: {
                name: 'div',
                classes: ['col-12', 'col-md-6', 'ck-twocolumns_first']
            },
        } );
        conversion.for( 'editingDowncast' ).elementToElement( {
            model: 'twoColumnFirst',
            view: ( modelElement, { writer: viewWriter } ) => {
                // Note: You use a more specialized createEditableElement() method here.
                const div = viewWriter.createEditableElement( 'div', { class: 'col-12 col-md-6 ck-twocolumns_first' } );

                return toWidgetEditable( div, viewWriter );
            }
        } );

        // <simpleBoxDescription> converters
        conversion.for( 'upcast' ).elementToElement( {
            model: 'twoColumnSecond',
            view: {
                name: 'div',
                classes: ['col-12', 'col-md-6', 'ck-twocolumns_second']
            }
        } );
        conversion.for( 'dataDowncast' ).elementToElement( {
            model: 'twoColumnSecond',
            view: {
                name: 'div',
                classes: ['col-12', 'col-md-6', 'ck-twocolumns_second']
            }
        } );
        conversion.for( 'editingDowncast' ).elementToElement( {
            model: 'twoColumnSecond',
            view: ( modelElement, { writer: viewWriter } ) => {
                // Note: You use a more specialized createEditableElement() method here.
                const div = viewWriter.createEditableElement( 'div', { class: 'col-12 col-md-6 ck-twocolumns_second'} );

                return toWidgetEditable( div, viewWriter );
            }
        } );
    }
}
