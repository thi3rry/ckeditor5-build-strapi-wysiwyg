import { Plugin } from '@ckeditor/ckeditor5-core';
import SimpleBoxUI from "./SimpleBoxUi";
import SimpleBoxEditing from "./SimpleBoxEditing";

export default class SimpleBox extends Plugin {
    static get requires() {
        return [ SimpleBoxEditing, SimpleBoxUI ];
    }
}
