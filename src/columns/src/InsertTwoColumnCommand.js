import Command from "@ckeditor/ckeditor5-core/src/command";



function createTwoColumn( writer ) {
    const twoColumn = writer.createElement( 'twoColumn' );
    const twoColumnFirst = writer.createElement( 'twoColumnFirst' );
    const twoColumnSecond = writer.createElement( 'twoColumnSecond' );

    writer.append( twoColumnFirst, twoColumn );
    writer.append( twoColumnSecond, twoColumn );

    // There must be at least one paragraph for the description to be editable.
    // See https://github.com/ckeditor/ckeditor5/issues/1464.
    writer.appendElement( 'paragraph', twoColumnFirst );
    writer.appendElement( 'paragraph', twoColumnSecond );

    return twoColumn;
}


export default class InsertTwoColumnCommand extends Command {
    execute() {
        this.editor.model.change( writer => {
            // Insert <twoColumn>*</twoColumn> at the current selection position
            // in a way that will result in creating a valid model structure.
            this.editor.model.insertContent( createTwoColumn( writer ) );
        } );
    }

    refresh() {
        const model = this.editor.model;
        const selection = model.document.selection;
        const allowedIn = model.schema.findAllowedParent( selection.getFirstPosition(), 'twoColumn' );

        this.isEnabled = allowedIn !== null;
    }
}
