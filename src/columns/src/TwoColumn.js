import { Plugin } from '@ckeditor/ckeditor5-core';
import TwoColumnUI from "./TwoColumnUi";
import TwoColumnEditing from "./TwoColumnEditing";
import '../theme/twocolumns.css';
export default class TwoColumn extends Plugin {
    static get requires() {
        return [ TwoColumnEditing, TwoColumnUI ];
    }
}
