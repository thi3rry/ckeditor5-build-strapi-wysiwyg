import * as axios from "axios";

/**
 * TODO handle pagination
 * TODO handle search
 */
export class EntityExplorer {
    constructor({
        enabled = true,
        contentTypes = [],
        baseUrl = 'http://localhost:3002'
    }) {
        this.enabled = enabled;
        this._onSelectCallbacks = [];
        this._contentTypes = contentTypes;
        this._baseUrl = baseUrl;
        this._entities = [];
        this._defaultEntityToStringCallback = (entity) => {
            return entity.title ? entity.title : (entity.name ? entity.name : (entity.libelle ? entity.libelle : entity.id))
        }

        let tpl  = `<span class="popin-explorer__overlay"></span>
                <div class="popin-explorer__container">
                    <div class="popin-explorer__head"><strong>Sélectionnez la cible du lien</strong></div>
                    <div class="popin-explorer__inner">
                        <div class="popin-explorer__types">
                            Type de contenu&nbsp;
                            <div class="js-popin-explorer__types"></div>
                        </div>
                        <div class="js-popin-explorer__content popin-explorer__content"></div>
                    </div>
                    <div class="popin-explorer__footer">
                        <button class="js-popin-explorer__close-btn popin-explorer__close-btn">Annuler</button> 
                        <button class="js-popin-explorer__submit-btn popin-explorer__submit-btn is-disabled">Valider</button>
                    </div>
                </div>`;
        var popinDom = document.createElement('div');
        popinDom.classList.add('popin-explorer');
        popinDom.classList.add('js-popin-explorer');
        popinDom.id = "popin-explorer";
        popinDom.innerHTML = tpl;
        document.body.appendChild(popinDom);

        this.noResultText = "<p class='centered-text'>Aucun contenu détecté pour le type de contenu sélectionné.</p>"
        this.loadingText = "<p class='centered-text'>Recherche en cours...</p>"

        this.popin = document.getElementById('popin-explorer');
        this.explorerContent = this.popin.querySelector('.js-popin-explorer__content');
        this.explorerTypes = this.popin.querySelector('.js-popin-explorer__types');
        this.explorerTypes.addEventListener('change', (e)=>{
            this.entityTypeSelectHandler(e.target.value);
        });
        this.closeBtn = this.popin.querySelector('.js-popin-explorer__close-btn');
        this.submitBtn = this.popin.querySelector('.js-popin-explorer__submit-btn');

        this.closeBtn.addEventListener('click', this.close.bind(this));
        this.submitBtn.addEventListener('click', this.submit.bind(this));

        this.initStyle();
    }

    getEntityConfiguration(id) {
        if (this._contentTypes.length === 0) {
            console.warn('No types configured', this._contentTypes);
        }
        return this._contentTypes.find(type => type.id === id);
    }

    /**
     * @param currentType
     * @param currentId
     * @param contentTypes
     */
    show({currentType = null, currentId = null, contentTypes = null}){
        if (contentTypes) {
            this._contentTypes = contentTypes;
        }

        this.popin.classList.add('is-visible');

        this.selectedEntityType = currentType;
        this.selectedEntityId = currentId;

        if(!this.selectedEntityType){
            this.explorerContent.innerHTML = this.noResultText;
        }

        this.refreshTypeSelector();

        if(this.selectedEntityType){
            this.entityTypeSelectHandler(this.selectedEntityType, true);
        }
    }

    refreshTypeSelector() {
        this.typeSelect = document.createElement('select');
        this.typeSelect.classList.add("js-popin-explorer__types__select");

        const optionDefault = document.createElement("option");
        optionDefault.value = null;
        optionDefault.text = 'Sélectionner la catégorie';
        if(this._contentTypes.length === 0) {
            console.warn('Aucun type de contenu paramétré.');
            optionDefault.text = 'Aucune catégorie définie';
        }
        this.typeSelect.appendChild(optionDefault);


        this._contentTypes.forEach((type) => {
            var option = document.createElement("option");
            option.value = type.id;
            option.text = type.label;
            if(type.id === this.selectedEntityType){
                option.selected = "selected";
            }
            this.typeSelect.appendChild(option);
        })

        this.explorerTypes.appendChild(this.typeSelect);

        if(this._contentTypes.length === 1) {
            this.explorerTypes.hide();
            this.selectedEntityType = this._contentTypes[0];
        }
    }

    renderError(err) {
        return `
            <div>
                <p class="centered-text">Une erreur est survenue :</p>
                <pre style="color: red; max-width: 100%;">${JSON.stringify(err)}</pre>
            </div>
        `;
    }

    entityTypeSelectHandler(__entity, __force = false){
        if(__entity && __entity !== "null") {
            this.selectedEntityType = __entity;
            this.explorerContent.innerHTML = this.loadingText;

            // TODO handle pagination
            this.find(__entity, {pageSize: 200, page: 1})
                .then(
                    ({data}) => {
                        if (data.length === 0) {
                            this.explorerContent.innerHTML = this.noResultText;
                        }
                        else {
                            this.updateSelectedList(data, __force);
                        }
                        console.log('data found', data);
                    },
                    (err) => {
                        this.explorerContent.innerHTML = this.renderError(err);
                        console.error('data rejected', err);
                    }
                )
                .catch((err) => {
                    this.explorerContent.innerHTML = this.renderError(err);
                    console.error('data error', err);
                })
            ;
        }else{
            this.selectedEntityType = null;
            this.explorerContent.innerHTML = this.noResultText;
        }
    }

    getEntityAsString(entity, typeId) {
        const type = this.getEntityConfiguration(typeId);
        if (type.toStringCallback) {
            try {
                return type.toStringCallback(entity);
            }
            catch (err) {
                console.warning('Impossible de transformer l\'entité en chaîne de caractère');
            }
        }

        return this._defaultEntityToStringCallback(entity);
    }

    updateSelectedList(entityList, __force = false){

        if(!__force) {
            this.selectedEntityId = null;
            if(!this.submitBtn.classList.contains('is-disabled')) this.submitBtn.classList.add('is-disabled');
        }
        if(entityList.length > 0) {
            this.explorerContent.innerHTML = null;
            let explorerContentList = document.createElement("div");
            explorerContentList.classList.add('popin-explorer__content__list');
            this.explorerContent.appendChild(explorerContentList);
            for (var i = 0; i < entityList.length; i++) {
                var label = document.createElement("label");
                var text = document.createElement("span");
                var input = document.createElement("input");
                input.type = "radio";
                input.value = entityList[i].id;
                input.name = 'entity';
                if(__force && this.selectedEntityId && entityList[i].id === this.selectedEntityId) {
                    input.checked = 'checked';
                    this.submitBtn.classList.remove('is-disabled');
                }
                input.addEventListener('change', (e) => {
                    if(e.target.checked) {
                        this.selectedEntityId = e.target.value;
                        this.submitBtn.classList.remove('is-disabled');
                    }
                });
                text.textContent = this.getEntityAsString(entityList[i], this.selectedEntityType);
                label.appendChild(input);
                label.appendChild(text);
                explorerContentList.appendChild(label);
            }
        } else {
            this.explorerContent.innerHTML = this.noResultText;
        }
    }

    close(){
        this.popin.classList.remove('is-visible');
        window.setTimeout(()=>{
            this.explorerContent.innerHTML = this.noResultText;
            this.explorerTypes.innerHTML = null;
        },500);
    }

    submit(){
        this._onSelectCallbacks.forEach(cb => cb(this.getEntityConfiguration(this.selectedEntityType), this.selectedEntityId));
        this.close();
    }

    /**
     * Add a callback to the onselect action of the explorer
     * @param {(function(type, id): void)} callback
     * @returns {(function(): void)|*}
     */
    onSelect(callback) {
        this._onSelectCallbacks.push(callback);
        return () => {
            this._onSelectCallbacks.splice(this._onSelectCallbacks.findIndex(callback), 1);
        }
    }

    axios() {
        return axios.create({
            baseUrl: process.env.VUE_APP_STRAPI_BASE_URL,
            headers: {...this.getAuthHeaders()},
            responseType: 'json'
        });
    }

    async find (entity, searchParams) {
        const entityType = this.getEntityConfiguration(entity);
        const {data} = await this.axios().get(`${this._baseUrl}/content-manager/collection-types/${entityType.id}`, { params: searchParams })
        return {
            data: data.results,
            pagination: data.pagination,
            type: entityType
        };

    }

    getAuthHeaders(){
        const token = JSON.parse(sessionStorage.getItem('jwtToken'));
        const Authorization = token ? `Bearer ${token}` : '';
        return Authorization ? { Authorization } : {};
    }

    initStyle(){
        const explorerStyle = document.createElement('style');
        explorerStyle.textContent = this.getStyle();
        document.head.append(explorerStyle);
    }

    getStyle(){
        return `
      .centered-text{
        font-size: 13px;
        text-align: center;
        padding: 0 1rem;
        display: flex;
        align-items: center;
        justify-content: center;
        flex: 1;
        height: 100%;
        opacity: 0.8;
      }
      .popin-explorer{
        position: fixed;
        z-index: 9999;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        align-items: center;
        justify-content: center;
        display: flex;
        pointer-events: none;
        opacity: 0;
        transition: opacity 0.3s linear;
      }
      .popin-explorer.is-visible{
        pointer-events: all;
        opacity: 1;
      }
      .popin-explorer__overlay {
        position: fixed;
        z-index: 9998;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0,0,0, 0.2);
      }
      .popin-explorer__container{
        position: relative;
        z-index: 10000;
        background: white;
        max-width: 100vw;
        width: 500px;
        height: 100vh;
        max-height: 500px;
        display: flex;
        flex-direction: column;
        box-shadow: 0 2px 5px rgba(0,0,0,0.1);
      }
      .popin-explorer__head{
        display: flex;
        justify-content: space-between;
        border-bottom: solid 1px #eee;
        padding: 1rem;
      }
      .popin-explorer__close{
        border: none;
        background: none;
        font-size: 12px;
      }
      .popin-explorer__inner{
        flex: 1;
        display: flex;
        flex-direction: column;
        height: 300px;
      }
      .popin-explorer__types{
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 0.5rem 1rem;
        font-size: 14px;
        font-weight: 500;
        border-bottom: solid 1px #eee;
      }
      .popin-explorer__content{
        flex: 1;
        height: 300px;
        overflow-y: scroll;
      }
      .popin-explorer__content label{
        display: flex;
        padding: 0.5rem 1rem;
        border-bottom: solid 1px #eee;
        font-size: 14px;
        font-weight: 500;
      }
      .popin-explorer__content input{
        margin-right: 1rem;
      }
      .popin-explorer__content label:nth-child(odd){
        background: rgb(250, 250, 251);
      }
      .popin-explorer__close-btn,
      .popin-explorer__submit-btn{
        font-weight: 600;
        border-radius: 2px;
        min-width: 120px;
        border: none;
        font-size: 13px;
        height: 28px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
      }
      .popin-explorer__close-btn {
        background: rgb(233, 234, 235);
      }

      .popin-explorer__submit-btn{
        background: rgb(0, 126, 255);
        color: white;
      }
      .popin-explorer__submit-btn.is-disabled{
        background: #aaa;
        opacity: 0.5;
        pointer-events: none;
        cursor: not-allowed;
      }
      .popin-explorer__footer{
        display: flex;
        justify-content: flex-end;
        padding: 1rem;
        background: white;
        border-top: solid 1px #eee;
      }
      .popin-explorer__footer > *{
        margin-left: 0.5rem;
      }
    `;
    }
}

